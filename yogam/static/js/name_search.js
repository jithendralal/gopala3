
setname = function(name){
    $("#id_name").val(name);
    $("#popup_body").html("")
    $("#popup").hide();
}

clearname = function(name){
    $("#id_name").val('').css('background','snow');
}

$(document).ready(function(){
    $("#popup").hide();
    $("#id_name").attr('placeholder','Type first four letters')
    $("#id_name").parent().append('<div class="small-link" onclick="clearname();">Clear Name</div>')

    $("#id_name").keyup(function(){
        var srch = $(this).val();
        if (srch.length>=4){
            $(this).blur();
            $.get("/family/name_search/?srch="+srch, function(data) {
                var found = data.indexOf("not-found");
                if (found==-1){
                    $('#popup_body').html(data);
                    $('#popup').show().center();
                    $("#id_name").css('background','snow');
                }else{
                    $("#id_name").css('background','pink').focus();
                }
            });
        }
    });

});