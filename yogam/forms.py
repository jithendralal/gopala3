from django import forms

from .models import NewsItem, Feedback


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = '__all__'
        exclude = ['reply']


class NewsForm(forms.ModelForm):
    class Meta:
        model = NewsItem
        fields = '__all__'
        exclude = ['is_public', 'pic_width', 'pic_height']
