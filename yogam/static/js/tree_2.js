
show_popup = function(id){
    $('#popup_body').html('');
    $('#popup_title_text').html($('#'+id).attr('title'));
    $('#popup_body').load("/family/get_member_data/?id="+id, function(){
        $('#member_image').fadeIn(1000);
        $('#popup').show().center();
    });
}

show_photo_popup = function(src, width){
    $('#photo_popup_image').attr('src','');
    $('#photo_popup_image').attr('src',src);
    $('#photo_popup_image').css('width', (width*50/100));
    $('#photo_popup').show().center();
}

$(document).ready(function(){
    $('.memberimg').click(function(){
        $('#loader').show().center();
        show_popup($(this).attr('id'));
    });

    $('#popup').hide();

    $('#popup_close').click(function(){
        $('#loader').hide();
        $('#popup').hide();
        $('#photo_popup').hide();
    });

    $('#photo_popup').hide();    

    $('#photo_popup_close').click(function(){
        $('#loader').hide();
        $('#photo_popup').hide();
    });

    $('body').on('click', '#member_image', function(){ 
        var width = $(window).width()
        show_photo_popup($(this).attr('src'), width);
    });

    $('.toggle').click(function(){
        var id = $(this).attr('id');
        var children = '.parent-' + id;
        $(children).toggle(100);
    });

});
