$(document).ready(function(){

    $("#add_button").click(function(){
        $("#add_form").show();
        $(this).hide();
    });

    $("#add_close").click(function(){
        $("#add_form").hide();
        $("#add_button").show();
    });

    $("#id_event_date").datepicker({ dateFormat: "yy-m-d" });

});
