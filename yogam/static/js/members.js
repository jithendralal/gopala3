$(document).ready(function(){
    $('.alpha-link').click(function() {
        $('.alpha-link-clicked').attr('class','alpha-link');
        $(this).attr('class','alpha-link-clicked');
        var srch = $(this).attr('id');
        $('.keys1').each(function() {
            var elem = $(this);
            var ht = $(elem).val();
            if (ht.search(srch)==0) {
                $(elem).parent().parent().show();
            } else{
                $(elem).parent().parent().hide();
            }
        });
    });
});
