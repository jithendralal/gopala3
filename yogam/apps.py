from django.apps import AppConfig


class YogamConfig(AppConfig):
    name = 'yogam'
