from django.urls import path


from . import views


app_name = 'yogam'
urlpatterns = [
    path('news_list/', views.news_list, name='news-list'),
    path('news/<int:pk>/', views.NewsDetailView.as_view(), name='news'),
    path('articles/', views.articles, name='articles'),
    path('article/<int:id>/', views.article, name='article'),
    path('photos/<int:id>/', views.photos, name='photos'),
    path('tree/', views.tree, name='tree'),
    path('get_member_data/', views.get_member_data, name='member-data'),
    path('births/<str:month>/', views.births, name='births'),
    path('members/', views.MembersView.as_view(), name='members'),
    path('blood_groups/', views.blood_groups, name='blood_groups'),
    path('member_list/', views.MemberList.as_view(), name='member-list'),
    path('committee/', views.committee, name='committee'),
    path('about/', views.AboutView.as_view(), name='about'),
    path('feedback/', views.feedback, name='feedback'),
    path('family/<int:id>/', views.family, name='family'),
    path('messages/', views.messages, name='messages'),
    path('name_search/', views.name_search, name='name-search'),
]
