from django import template
register = template.Library()


@register.inclusion_tag('yogam/blood.html')
def blood_graph(takes_context=True):
    groups = [(36, 'O+'), (30, 'B+'), (21, 'A+'), (8, 'AB+'), (4, 'O-'),
              (3, 'B-'), (2, 'A-'), (1, 'AB-')]
    blood = '<div id="blood">General Availability<br>'
    for g in groups:
        blood += '<span class="blood">' + '&nbsp;' * g[0] + '</span> ' \
                 + g[1] + '<br>'
    blood += '<div style="text-align:right;">Approximate Graph</div></div>'
    return {'blood': blood}
