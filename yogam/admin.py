from django.utils.safestring import mark_safe
from django.contrib import admin
from django.conf import settings
from django import forms
from .models import NewsItem
from .models import ArticlePhoto
from .models import Article
from .models import Member
from .models import Banner
from .models import Feedback
from .models import Accessed_IP
from .models import MALAYALAM_MONTHS, MALAYALAM_STARS


class ArticlePhotoInline(admin.StackedInline):
    model = ArticlePhoto


class ArticleAdmin(admin.ModelAdmin):
    model = Article
    list_display = ['title', 'author', 'published_date', 'is_public',
                    'home_page']
    inlines = [ArticlePhotoInline]

    class Meta:
        ordering = ['-published_date']


class MemberAdmin(admin.ModelAdmin):
    model = Member
    list_display = ['nick_name', 'ico', 'birth', 'addr', 'offaddr',
                    'parent', 'spouse']
    search_fields = ['name', 'nick_name']

    fieldsets = [('', {'fields': [('name', 'nick_name'), 'role',
                                  'contact_number', 'email', 'blood_group',
                                  'date_of_birth',
                                  ('birth_star', 'birth_month'), 'wedding']}),
                 ('Position in the family (leave blank for spouse)',
                  {'fields': ['parent', 'sibling_order', 'spouse',
                              'generation'],
                   'classes': ('collapse', )}),
                 ('Other information', {'fields': ['picture', 'icon',
                                                   'address',
                                                   'official_address',
                                                   'qualification',
                                                   'job', 'location',
                                                   'twitter', 'facebook'],
                                        'classes': ('collapse', )})]

    def birth(self, obj=None):
        ret = ''
        if obj.date_of_birth:
            ret += 'DOB: ' + obj.date_of_birth.strftime('%d-%m-%Y') + '<br>'
        if obj.birth_star:
            ret += 'Star: ' + MALAYALAM_STARS[obj.birth_star] + '<br>'
        if obj.birth_month:
            ret += 'Month: ' + MALAYALAM_MONTHS[obj.birth_month] + '<br>'
        if obj.blood_group:
            ret += 'Blood Group: ' + obj.blood_group + '<br>'
        return ret
    birth.short_description = 'Birth'
    birth.allow_tags = True

    def addr(self, obj=None):
        ret = ''
        if obj.address:
            ret = mark_safe(obj.address.replace('<p>', '')
                            .replace('</p>', '<br>'))
        return ret
    addr.short_description = 'Address'
    addr.allow_tags = True

    def offaddr(self, obj=None):
        ret = ''
        if obj.official_address:
            ret = mark_safe(obj.official_address.replace('<p>', '')
                            .replace('</p>', '<br>'))
        return ret
    offaddr.short_description = 'Office'
    offaddr.allow_tags = True

    def get_form(self, request, obj=None, **kwargs):
        form = super(MemberAdmin, self).get_form(request, obj, **kwargs)
        queryset = Member.objects.filter(generation__isnull=False)
        if obj:
            queryset = queryset.exclude(pk=obj.id)
        form.base_fields['parent'].queryset = queryset
        form.base_fields['spouse'].queryset = Member.objects\
            .filter(generation__isnull=True)
        return form

    def ico(self, obj=None):
        ret = ''
        if obj and obj.icon:
            ret = '<img style="width:50px;" src="' + obj.icon.url + '" alt="">'
        ret += '<br>' + obj.name
        if obj.role:
            ret += '<br>(' + obj.role + ' GVKY)'
        if obj.contact_number:
            ret += '<br>' + obj.contact_number
        if obj.email:
            ret += '<br>' + obj.email
        if obj.facebook:
            ret += '<br> <a href="' + obj.facebook + '">Facebook</a>'
        if obj.job:
            ret += '<br>' + obj.job
        return ret
    ico.allow_tags = True
    ico.short_description = 'Name'


class NewsAdmin(admin.ModelAdmin):
    model = NewsItem
    list_display = ['title', 'event_date', 'is_public']


class BannerAdmin(admin.ModelAdmin):
    model = Banner
    list_display = ['title', 'url']


class FeedbackAdmin(admin.ModelAdmin):
    model = Feedback
    list_display = ['subject', 'name', 'picture']


class Accessed_IPAdmin(admin.ModelAdmin):
    model = Accessed_IP
    list_display = ['ip', 'city', 'last_date']


admin.site.register(NewsItem, NewsAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Member, MemberAdmin)
admin.site.register(Banner, BannerAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(Accessed_IP, Accessed_IPAdmin)
