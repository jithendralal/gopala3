import datetime
from cgi import escape

from django.db.models import Q
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from django.template import Context
from django.template.loader import get_template
from django.contrib.gis.geoip2 import GeoIP2
from django.views import View
from django.views.generic import TemplateView, ListView
from django.views.generic.detail import DetailView

from .forms import NewsForm, FeedbackForm
from .models import NewsItem as News
from .models import Article, ArticlePhoto, Member, MONTH_LIST, Feedback, \
    NewsItem, Accessed_IP, Banner


# recursively generate the tree from a root
def make_tree(root):
    children = root.children()
    child_list = []
    if children.count():
        for child in children:
            tree = make_tree(child)
            child_list.append(tree)
    return {root: child_list}


# handle any attachments from the news form or feedback form
def handle_uploaded_file(f):
    destination = open(settings.MEDIA_ROOT + f.name, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    return


# try to get the ip of the client
def get_client_ip(request):
    ip = ''
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR', None)
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR', '')
    return ip


# update the last accessed date of an ip existing in the database
def update_ip(request):
    accessed_ip = None
    today = datetime.date.today()
    ip = get_client_ip(request)
    if ip:
        g = GeoIP2()
        city = None
        try:
            city = g.city(ip)
        except Exception as e:
            pass

        accessed_ips = Accessed_IP.objects.filter(ip=ip)

        location = ''
        if city is not None:
            location = ''
            if 'city' in city and city['city'] is not None:
                location += city['city']
            if 'region' in city and city['region'] is not None:
                location += ' ' + city['region']
            if 'country_name' in city and city['country_name'] is not None:
                location += ' ' + city['country_name']

        if accessed_ips.count():
            accessed_ip = accessed_ips[0]
            if accessed_ip.last_date != today:
                accessed_ip.last_date = today
                accessed_ip.city = location
                accessed_ip.save()
        else:
            accessed_ip = Accessed_IP(ip=ip, city=location)
            accessed_ip.save()
    return accessed_ip


def index(request):
    accessed_ip = update_ip(request)
    home_articles = Article.objects.filter(home_page=True)\
        .values('title', 'id')
    context = {'articles': articles, 'accessed_ip': accessed_ip}
    today = datetime.date.today()
    members = Member.objects.filter(date_of_birth=today)
    if members.count():
        context['members'] = members
    banners = Banner.objects.all().order_by('-id')
    if banners.count():
        context['banner'] = banners[0]
    return render(request, 'yogam/index.html', context)


class AboutView(TemplateView):
    template_name = "yogam/about.html"


# printable member list
class MemberList(ListView):
    model = Member


# the members list page
class MembersView(View):
    template_name = "yogam/members.html"

    def get(self, request):
        members = Member.objects.all().order_by('nick_name')\
            .values('nick_name', 'contact_number', 'icon', 'name', 'job',
                    'role', 'address', 'official_address', 'email')

        alpha = []
        for m in members:
            letter = m['nick_name'][0].lower()
            if letter not in alpha:
                alpha.append(letter)

        context = {'members': members, 'letters': alpha}
        return render(request, self.template_name, context)


def news_list(request):
    accessed_ip = update_ip(request)
    message = ''
    error = ''
    form = NewsForm()
    if request.method == 'POST':
        form = NewsForm(request.POST, request.FILES)
        message = 'News not saved.'
        error = True
        if form.is_valid():
            form.save()
            message = 'News saved.'
            error = False
            try:
                picture = request.FILES['picture']
                handle_uploaded_file(picture)
            except Exception as e:
                pass
            form = NewsForm()
    news = News.objects.filter(is_public=True).order_by('-event_date')
    context = {'news_list': news, 'form': form, 'message': message,
               'error': error, 'accessed_ip': accessed_ip}
    return render(request, 'yogam/news_list.html', context)


class NewsDetailView(DetailView):
    model = News


def messages(request):
    messages = Feedback.objects.order_by('-id')
    context = {'messages': messages}
    return render(request, 'yogam/messages.html', context)


def articles(request):
    articles = Article.objects.filter(is_public=True).exclude(home_page=True)\
        .order_by('-published_date')
    context = {'articles': articles}
    return render(request, 'yogam/articles.html', context)


@login_required
def article(request, id=None):
    article = Article.objects.get(pk=id)
    context = {'article': article}
    template = 'yogam/article.html'
    count = article.photo_count()
    if count:
        template = 'yogam/album.html'
        photo = ArticlePhoto.objects.filter(article_id=id).order_by('id')[0]
        context['photo'] = photo
        context['photos'] = ArticlePhoto.objects.filter(article_id=id)\
            .order_by('id').values('picture', 'id')
    return render(request, template, context)


def photos(request, photo_id=None):
    next = request.GET.get('next', None)
    prev = request.GET.get('prev', None)
    photo = ArticlePhoto.objects.get(pk=photo_id)
    photo1 = None
    try:
        if prev:
            photo1 = ArticlePhoto.objects.filter(article=photo.article)\
                .order_by('id').filter(id__lt=photo_id).last()
        elif next:
            photo1 = ArticlePhoto.objects.filter(article=photo.article)\
                .order_by('id').filter(id__gt=photo_id).first()
    except Exception as e:
        pass
    if photo1:
        photo = photo1
    article = photo.article
    context = {'photo': photo, 'article': article}
    return render(request, 'yogam/album.html', context)


@login_required
def get_photo(request):
    next = request.GET.get('next', None)
    prev = request.GET.get('prev', None)
    photo_id = request.GET.get('photo_id', None)
    photo = ArticlePhoto.objects.get(pk=photo_id)
    photo1 = None
    try:
        if prev:
            photo1 = ArticlePhoto.objects.filter(article=photo.article)\
                .order_by('id').filter(id__lt=photo_id).last()
        elif next:
            photo1 = ArticlePhoto.objects.filter(article=photo.article)\
                .order_by('id').filter(id__gt=photo_id).first()
    except Exception as e:
        pass
    if photo1:
        photo = photo1
    article = photo.article
    context = {'photo': photo, 'article': article}
    return render(request, 'yogam/album.html', context)


# recursively traverse a tree and render it in html
def traverse_tree(child_list):
    html = ''
    if len(child_list):
        for child_dict in child_list:
            member = child_dict.keys()[0]
            div = ''
            member_generation = member.generation
            generation = member_generation - 1
            margin = generation * 10

            if member_generation > 2:
                div += '<div class="node-row %s" style="display:none;">' % \
                       member.parent_ids()
            else:
                div += '<div class="node-row %s">' % member.parent_ids()

            i = 1
            while i < member_generation:
                if member.is_youngest():
                    if i == member_generation - 2 \
                            and member.parent.is_youngest() \
                            or i == member_generation - 3 \
                            and member.parent.parent.is_youngest():
                        div += '<span class="node-connector gen" ' \
                               'style="margin-left:20px;"></span>'
                    else:
                        div += '<span class="node-connector" ' \
                               'style="margin-left:20px;"></span>'
                else:
                    if i == member_generation - 2 \
                            and member.parent.is_youngest() \
                            or i == member_generation - 3 \
                            and member.parent.parent.is_youngest():
                        div += '<span class="node-connector gen" ' \
                               'style="margin-left:20px;"></span>'
                    else:
                        div += '<span class="node-connector" ' \
                               'style="margin-left:20px;"></span>'
                i += 1

            div += '<span class ="gen%s-conn">&nbsp;</span>' % \
                   member_generation

            div += '<span class="node-container">'

            div += '<span class="node-span">'
            div += '<span class="gen%s">' % member_generation

            if member_generation == 2:
                div += '<span class="toggle" id="%s">' % \
                       member.id + member.nick_name + '</span>'
            else:
                div += member.nick_name

            div += ' <img id="%s" class="memberimg" title="%s" ' \
                   'style="width:20px;" src="%s" alt="pic">' % \
                   (member.id, member.nick_name, member.icon.url)
            div += '</span>'
            div += '</span>'

            spouse = member.spouse
            if spouse:
                div += '<span class="node-span">'
                div += '<span class="gen%s">' % member_generation
                div += spouse.nick_name
                if spouse.icon:
                    div += ' <img id="%s" class="memberimg" title="%s" ' \
                           'style="width:20px;" src="%s" alt=""> ' % \
                           (spouse.id, spouse.nick_name, spouse.icon.url)
                div += '</span>'
                div += '</span>'

            div += '</span>'

            div += '</div>'

            tr_list = child_dict[member]
            html += div
            html += traverse_tree(tr_list)
    return html


def tree(request):
    html = '<div class="node-row">'
    root_members = Member.objects.filter(generation=1)
    if root_members.count():
        root_member = root_members[0]
        tree = make_tree(root_member)

        html += '<span class="node-container">'

        html += '<span class="node-span gen1">%s' % root_member.nick_name
        html += ' <img id="%s" class="memberimg" title="%s" ' \
                'style="width:20px;" src="%s" alt="pic">' % \
                (str(root_member.id), root_member.nick_name,
                 root_member.icon.url)
        html += '</span>'

        spouse = root_member.spouse
        if spouse:
            html += '<span class="node-span gen1">'
            html += spouse.nick_name
            html += ' <img id="%s" class="memberimg" title="%s" ' \
                    'style="width:20px;" src="%s" alt=""> ' % \
                    (spouse.id, spouse.nick_name, spouse.icon.url)
            html += '</span>'

        html += '</span>'

        html += '</div>'

        html += traverse_tree(tree[root_member])
    else:
        html += '</div>'
    context = {'tree': html}
    return render(request, 'yogam/tree.html', context)


# ajax method that returns the member details on memmber lists
def get_member_data(request):
    id = request.GET.get('id')
    member = Member.objects.get(pk=id)
    context = {'member': member}
    return render(request, 'yogam/member_data.html', context)


# ajax method to show suggestions on feedback form
def name_search(request):
    srch = request.GET.get('srch')
    members = Member.objects.filter(Q(name__icontains=srch) |
                                    Q(nick_name__icontains=srch))
    context = {'members': members}
    return render(request, 'yogam/name_search.html', context)


# get last date of a month
def get_month_end(year, month):
    day = 31
    end = None
    while 1:
        try:
            end = datetime.datetime(year, month, day, 23, 59, 59)
            break
        except Exception as e:
            day -= 1
    return end


# create a calendar with empty cells before day 1
# this will be rendered like an actual calendar
def create_calendar(year, month):
    calendar = {}
    start = datetime.datetime(year, month, 1)
    startday = start.weekday()
    empty_days = []
    if startday < 6:
        empty_days = range(0, startday + 1)
    end = get_month_end(year, month)
    cur = start.date()
    while cur <= end.date():
        calendar[cur] = []
        cur += datetime.timedelta(days=1)
    return start, empty_days, calendar


# add members to the calendar days if their DOB is on that day
def populate_calendar(calendar, members, current_year):
    for member in members:
        day = datetime.date(current_year, member['dob_month'],
                            member['dob_day'])
        calendar[day].append(member)
    return calendar


# the birth days page
def births(request, month=None):
    today = datetime.date.today()
    current_month = today.month
    current_year = today.year
    members = None
    calendar = None
    empty_days = 0
    start = today

    if month == 'init':
        month = current_month
    else:
        try:
            month = int(month)
        except Exception as e:
            pass

    if month in range(1, 13):
        members = Member.objects.filter(
            date_of_birth__isnull=False, dob_month=month)\
            .order_by('dob_month', 'dob_day', 'dob_year').values(
            'nick_name', 'date_of_birth', 'dob_monthname', 'icon',
            'name', 'dob_month', 'dob_day', 'dob_year')
        start, empty_days, calendar = create_calendar(current_year, month)
        calendar = populate_calendar(calendar, members, current_year)
        calendar = sorted(calendar.items())
    elif month == 'all':
        members = Member.objects.filter(date_of_birth__isnull=False)\
            .order_by('dob_month', 'dob_day', 'dob_year')\
            .values('nick_name', 'date_of_birth', 'dob_monthname', 'icon',
                    'name', 'dob_month', 'dob_day', 'dob_year')

    months = {}
    for m in range(1, 13):
        month_day = datetime.date(current_year, m, 1)
        months[m] = month_day

    context = {'members': members, 'current_month': current_month,
               'today': today,
               'current_year': current_year, 'selected': month,
               'calendar': calendar, 'empty_days': empty_days,
               'start': start, 'months': months}
    return render(request, 'yogam/births.html', context)


# blood groups page
@login_required
def blood_groups(request):
    members = Member.objects.exclude(blood_group='')
    group_list = ['O+', 'B+', 'A+', 'AB+', 'O-', 'B-', 'A-', 'AB-']
    groups = {'O+': [[], 0, 'opos'],
              'B+': [[], 0, 'bpos'],
              'A+': [[], 0, 'apos'],
              'AB+': [[], 0, 'abpos'],
              'O-': [[], 0, 'oneg'],
              'B-': [[], 0, 'bneg'],
              'A-': [[], 0, 'aneg'],
              'AB-': [[], 0, 'abneg']}
    for member in members:
        if member.blood_group:
            groups[member.blood_group][0].append(member)
            groups[member.blood_group][1] += 1

    blood = '<div id="blood">Our Family<br>'
    for key in group_list:
        g = groups[key]
        if g[1]:
            blood += '<span class="blood">' + '&nbsp;' * g[1] + '</span> ' + \
                     key + ' (' + str(g[1]) + ')<br>'
    blood += '<div style="text-align:right;">Approximate Graph</div></div>'

    context = {'groups': groups, 'keys': group_list, 'graph': blood}
    return render(request, 'yogam/blood_groups.html', context)


# handle feedback form POST
@login_required
def feedback(request):
    message = ''
    form = FeedbackForm()
    if request.method == 'POST':
        form = FeedbackForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            try:
                picture = request.FILES['picture']
                handle_uploaded_file(picture)
            except Exception as e:
                pass
            message = 'Thank you for your message. ' \
                      'Your message is listed in: ' \
                      '<b><a href="/yogam/messages/">Messages</a></b>'
            form = FeedbackForm()
    context = {'form': form, 'message': message}
    return render(request, 'yogam/feedback.html', context)


# the family of a member from the list page
def family(request, root_id):
    root = Member.objects.get(pk=root_id)
    tree = make_tree(root)
    context = {'family': tree}
    return render(request, 'yogam/family.html', context)


# the yogam committee page
def committee(request):
    presidents = Member.objects.filter(role='President')
    vices = Member.objects.filter(role='Vice President')
    secretaries = Member.objects.filter(role='Secretary')
    joints = Member.objects.filter(role='Joint Secretary')
    treasurers = Member.objects.filter(role='Treasurer')
    members = Member.objects.filter(role='Committee')
    context = {'members': members, 'pres': presidents, 'vice': vices,
               'secy': secretaries, 'jsec': joints, 'trea': treasurers}
    return render(request, 'yogam/committee.html', context)
